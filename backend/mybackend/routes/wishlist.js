const express = require('express')
const router = express.Router()
const wishlistController = require('../controller/wishlistController')
const Wishlist = require('../models/Wishlist')


// router.get('/', jobController.getJobs)

router.get('/:id', wishlistController.getwishlistIdUser)
router.post('/', wishlistController.addtowishlist)
router.delete('/:id' ,wishlistController.deletewishlist)




module.exports = router