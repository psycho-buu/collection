const express = require('express')
const router = express.Router()
const companyController = require('../controller/companyController')
// const Company = require('../models/Company')

router.get('/', companyController.getCompanies)
router.get('/:id',companyController.getCompany)
router.post('/', companyController.addCompany)
router.put('/',  companyController.updateCompany)
router.delete('/:id' ,companyController.deleteCompany)


// router.get('/', function (req, res, next) {

//   res.json(companyController.getCompanys())
//    Company.find({}).then(function (companys) {
//      res.json(companys)
//    }).catch(function (err) {
//      res.status(500).send(err)
//    })
// })

// router.get('/:id', (req, res, next) => {
//   const { id } = req.params
//   res.json(companyController.getCompany(id))
//    Company.findById(id).then(function(company) {
//      res.json(company)
//      console.log(company)
//    }).catch(function (err) {
//      res.status(500).send(err)
//    })
// })

// router.post('/', async (req, res, next) => {
//   const payload = req.body
//   res.json(companyController.addNewCompany(payload))

//    const company = new Company(payload)
//    try {
//      await company.save()
//      res.json(company)
//    } catch(err){
//      res.status(500).send(err)
//    }
// })

// router.put('/', async (req, res, next) => {
//   const payload = req.body
//   res.json(companyController.updateCompany(payload))

//    try {
//      const company = await Company.updateOne({ _id: payload._id}, payload)
//      res.json(company)
//    } catch(err) {
//      res.status(500).send(err)
//    }
// })

// router.delete('/:id', async (req, res, next) => {
//   const { id } = req.params
//   res.json(companyController.deleteCompany(id))

//    try {
//      const company = await Company.deleteOne( {_id: id} )
//      res.json(company)
//    } catch(err) {
//      res.status(500).send(err)
//    }
// })

module.exports = router
