var express = require('express');
var router = express.Router();
var jobPostController = require('../controller/jobPostController')
const JobPost = require('../models/JobPost')

router.get('/', jobPostController.getJobPosts)
router.get('/:id', jobPostController.getJobPost)
router.post('/', jobPostController.addJobPost)
router.put('/', jobPostController.updateJobPost)
router.delete('/:id' ,jobPostController.deleteJobPost)

module.exports = router;