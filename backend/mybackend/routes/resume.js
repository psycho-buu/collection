var express = require('express');
var router = express.Router();
var ResumeController = require('../controller/resumeController')
// const Resume = require('../models/Resume')

router.get('/', ResumeController.getResumes)
router.get('/:id',ResumeController.getResume)
router.post('/', ResumeController.addResume)
router.put('/',  ResumeController.updateResume)

module.exports = router;