const express = require('express')
const router = express.Router()
const profilesController = require('../controller/ProfileController')
const Profile = require('../models/Profile')


router.get('/', profilesController.getProfiles)
router.get('/:id', profilesController.getProfile)
router.post('/', profilesController.addProfile)
router.put('/',profilesController.updateProfile)
router.delete('/',profilesController.deleteProfile)

module.exports = router
