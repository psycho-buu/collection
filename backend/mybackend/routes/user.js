const express = require('express')
const router = express.Router()
const userController = require('../controller/userController')
const User = require('../models/User')

router.get('/', (req, res, next) => {

  // res.json(userController.getUsers())

  // Pattern 1
  // User.find({}).exec(function (err, users) {
  //   if(err) {
  //     res.status(500).send()
  //   }
  //   res.json(users)
  // })

  // Pattern 2
  User.find({}).then(function (users) {
    res.json(users)
  }).catch(function(err) {
    res.status(500).send(err)
  })

  //Pattern 3 : ASYN AWAIT
  // try{
  //   const users = User.find({})
  //   res.json(users)
  // } catch (err) {
  //   res.status(500).send(err)
  // }

})

router.get('/:id', function (req, res, next) {
  const { id } = req.params
  // res.json(userController.getUser(id))
  
  //Pattern 1
  User.findById(id).then(function(user) {
    res.json(user)
    console.log(user)
  }).catch(function (err) {
    res.status(500).send(err)
  })

  //Pattern 2 : ASYNC AWAIT
  // try {
  //   const user = await User.findById(id)
  //   res.json(user)
  // } catch (err) {
  //   res.status(500).send(err)
  // }
})

router.post('/', async (req, res, next) => {
  const payload = req.body
  console.log(payload)
  // res.json(userController.addUser(payload))

  //Pattern 1
  // User.create(payload).then(function(user){
  //   res.json(user)
  // }).catch(function (err) {
  //   res.status.send(err)
  // })

  //Pattern 2 : ASYNC AWAIT
  const user = new User(payload)
  try {
    await user.save()
    res.json(user)
  } catch(err) {
    res.status(500).send(err)
  }
  
})

router.post('/login', async (req, res, next) => {
  const payload = req.body
  console.log(payload)
  // res.json(userController.addUser(payload))

  User.findOne(payload).then(function(user) {
    res.json(user)
    console.log(user)
  }).catch(function (err) {
    res.status(500).send(err)
  })
  
})


router.put('/', async (req, res, next) => {
  const payload = req.body

  // res.json(userController.updateUser(payload))

  //Pattern 1
  try {
    const user = await User.updateOne({ _id: payload._id }, payload)
    res.json(user)
  } catch(err) {
    res.status(500).send(err)
  }
})

router.delete('/:id', async (req, res, next) => {
  const { id } = req.params
  // res.json(userController.deleteUser(id))

  //AWAIT ASYNC
  try {
    const user = await User.deleteOne( {_id: id })
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

// router.get('/:message', (req, res, next) => {
//   const { params } = req
//   res.json({ message: 'USER PAGE!', params })
// })

module.exports = router
