const express = require('express')
const router = express.Router()
const companyProfileController = require('../controller/companyProfileController')

router.get('/', companyProfileController.getProfiles)
router.get('/:id', companyProfileController.getProfile)
router.post('/', companyProfileController.addProfile)
router.put('/',companyProfileController.updateProfile)
router.delete('/',companyProfileController.deleteProfile)

module.exports = router
