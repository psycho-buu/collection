var express = require('express');
var router = express.Router();
var positionController = require('../controller/positionController')
// const Position = require('../models/Position')

router.get('/', positionController.getPositions)
router.get('/:id',positionController.getPosition)
router.post('/', positionController.addPosition)
router.put('/',  positionController.updatePosition)
router.delete('/:id' ,positionController.deletePosition)



// router.get('/', function(req, res, next) {
//   pattern1
//   Position.find({}).exec(function (err, positions){
//     if (err){
//       res.status(500).send()
//     }
//     res.json(positions)
//   })
//    pattern2
//   Position.find({}).then(function (positions){
//     res.json(positions)
//   }).catch(function (err){
//     res.status(500).send(err)
//   })

//   Pattern 3 
//   try{
//     const positions = await Position.find({})
//     res.json(positions)
//   } catch (err) {
//     res.status(500).send(err)
//   }
// });

// router.get('/:id', function(req, res, next) {
//   const { id } = req.params
//    res.json(positionController.getUser(id))
//   Position.findById(id).then(function(Position) {
//     res.json(Position)
//   }).catch(function (err) {
//     res.status(500).send(err)
//   })
// });

// router.post('/', async (req, res, next) => {
//   const payload = req.body
//   // res.json(positionController.addUser(payload))
//   const position = new Position(payload)
//   try {
//     await position.save()
//     res.json(position)
//   } catch(err) {
//     res.status(500).send(err)
//   }
// })

// router.put('/', async(req, res, next) => {
//   const payload = req.body
//  // res.json(positionController.updateUser(payload))
//  try {
//   const position = await Position.updateOne({ _id: payload._id }, payload)
//   res.json(position)
// } catch(err) {
//   res.status(500).send(err)
// }
// })

// router.delete('/:id', async (req, res, next) => {
//   const { id } = req.params
//   //res.json(positionController.deleteUser(id))
//   try {
//     const position = await Position.deleteOne( {_id: id })
//     res.json(position)
//   } catch (err) {
//     res.status(500).send(err)
//   }
// })

module.exports = router;
