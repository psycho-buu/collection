const express = require('express')
const router = express.Router()
const applicantController = require('../controller/applicantController')
const Applicant = require('../models/Applicant')




router.get('/:id', applicantController.getApplicantIdUser)
router.put('/', applicantController.updateApplicant)
router.post('/', applicantController.addApplicant)
router.delete('/:id' ,applicantController.deleteApplicant)




module.exports = router