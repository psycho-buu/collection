const express = require('express')
const router = express.Router()
const jobController = require('../controller/jobController')
const Job = require('../models/Job')


// router.get('/', jobController.getJobs)
router.get('/', jobController.getJobs)
router.get('/:id', jobController.getJobId)
router.post('/', jobController.getJob)
router.put('/', jobController.updateJob)
router.delete('/:id' ,jobController.deleteJob)




module.exports = router