var express = require('express');
var cors = require('cors')
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var positionRouter = require('./routes/position');
const profilesRouter = require('./routes/profiles')
var userRouter = require('./routes/user')
var jobRouter = require('./routes/job')
var jobPostRouter = require('./routes/jobpost')
var companyRouter = require('./routes/company')
var companyProfileRouter = require('./routes/companyprofile')
var resumeRouter = require('./routes/resume')
var wishlistRouter = require('./routes/wishlist')
var applicantRouter = require('./routes/applicant')


//Import the mongoose module
var mongoose = require('mongoose');

//Set up default mongoose connection
var mongoDB = 'mongodb://127.0.0.1/my_database';
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

//Get the default connection
var db = mongoose.connection;

//Bind connection to nerror event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())

app.use('/', indexRouter);
app.use('/position', positionRouter);
app.use('/profiles', profilesRouter);
app.use('/user', userRouter);
app.use('/job', jobRouter);
app.use('/jobpost', jobPostRouter);
app.use('/company', companyRouter);
app.use('/companyprofile', companyProfileRouter);
app.use('/resume', resumeRouter);
app.use('/wishlist', wishlistRouter);
app.use('/applicant', applicantRouter);



module.exports = app;
