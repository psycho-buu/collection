//Import the mongoose module
var mongoose = require('mongoose');
const User = require('./models/User')
const Profile = require('./models/Profile')
const Position = require('./models/Position')
const Company = require('./models/Company')
const CompanyProfile = require('./models/CompanyProfile')

//Set up default mongoose connection
var mongoDB = 'mongodb://127.0.0.1/my_database';
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//  Position.find(function (err, positions) {
//      if(err) return console.error(err)
//       console.log(positions)
//  })

// User.find(function (err, users) {
//     if(err) return console.error(err)
//     console.log(users)
// })

//  Profile.find(function (err, users) {
//      if(err) return console.error(err)
//      console.log(users)
//      })

  // Company.find(function (err, companies) {
  //   if(err) return console.error(err)
  //   console.log(companies)
  // })

  // CompanyProfile.find(function (err, companyprofiles) {
  //   if(err) return console.error(err)
  //   console.log(companyprofiles)
  // })