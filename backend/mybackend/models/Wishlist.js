const mongoose = require('mongoose')
const Schema = mongoose.Schema
const wishlistSchema = new Schema({
    id_user: String,
    id_jobpost: String
})

module.exports = mongoose.model('Wishlist', wishlistSchema);