const mongoose = require('mongoose')
const Schema = mongoose.Schema
const jobPostSchema = new Schema({
  jobType: String,
  position: String,
  minSalary: Number,
  maxSalary: Number,
  open: String,
  status: Boolean,
  amount: Number,
  military: String,
  gender: String,
  detail: String,
  property: String,
  welfare: String,
  id_user: String
})
module.exports = mongoose.model('JobPost', jobPostSchema)