const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
    id: Number,
    name: String,
    password: String,
    email: String,
    tel: String,
    typeuser: String
})

module.exports = mongoose.model('User', userSchema)
