const mongoose = require('mongoose')
const Schema = mongoose.Schema
const companySchema = new Schema({
  id: Number,
  company_name: String,
  type: String,
  address: String,
  subdistrict: String,
  district: String,
  province: String,
  postalnumber: String,
  tel: String,
  email: String,
  website: String,
  line: String
})

module.exports = mongoose.model('Company', companySchema)
