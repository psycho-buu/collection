const mongoose = require('mongoose')
const Schema = mongoose.Schema
const companyprofileSchema = new Schema({
  company_name: String,
  typejob: String,
  address: String,
  subdistrict: String,
  district: String,
  province: String,
  postalnumber: String,
  tel: String,
  email: String,
  website: String,
  line: String,
  id_user: String,
})

module.exports = mongoose.model('companyProfile', companyprofileSchema)