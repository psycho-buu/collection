const mongoose = require('mongoose')
const Schema = mongoose.Schema
const applicantSchema = new Schema({
    id_user: String,
    id_jobpost: String,
    status: String,
    detail: String
})

module.exports = mongoose.model('Applicant', applicantSchema)