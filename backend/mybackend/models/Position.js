const mongoose = require('mongoose')
const Schema = mongoose.Schema
const positionSchema = new Schema({
    id: Number,
    name: String,
    minSalary: Number,
    maxSalary: Number
})

module.exports = mongoose.model('Position', positionSchema)