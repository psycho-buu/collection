const mongoose = require('mongoose');
const Schema = mongoose.Schema
const profileSchema = new Schema({
    first_name: String,
    last_name
        :
        String,
    birth_date
        :
        String,
    sex
        :
        String,
    nationality
        :
        String,
    religion
        :
        String,
    address
        :
        String,
    province
        :
        String,
    district
        :
        String,
    subdistrict
        :
        String,
    postalnumber
        :
        String,
    tel
        :
        String,
    email: String,
    id_user: String,


})
module.exports = mongoose.model('Profile', profileSchema)