const Position = require('../models/Position')
const positionController = {
  positions: [
    // { id: 1, name: 'งานบัญชี', minSalary: '5000', maxSalary: '10000' },
    // { id: 2, name: 'คอมพิวเตอร์/IT/โปรแกรมเมอร์', minSalary: '5000', maxSalary: '10000' }
  ],
  lastId: 3,
  async addPosition(req, res, next) {
    const payload = req.body
    const position = new Position(payload)
    try {
      await position.save()
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updatePosition(req, res, next) {
    const payload = req.body
    try {
      const position = await Position.updateOne({ _id: payload._id }, payload)
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deletePosition(req, res, next) {
    const { id } = req.params
    try {
      const position = await Position.deleteOne({ _id: id })
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPositions(req, res, next) {
    try {
      const positions = await Position.find({})
      res.json(positions)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  getPosition(req, res, next) {
    const { id } = req.params
    Position.findById(id).then(function (Position) {
      res.json(Position)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
}

module.exports = positionController