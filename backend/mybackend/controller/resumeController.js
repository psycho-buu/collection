const Resume = require('../models/Resume')
const resumeController = {
  resumes: [
  ],
  lastId: 3,
  async addResume(req, res, next) {
    const payload = req.body
    const resume = new Resume(payload)
    try {
      await resume.save()
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateResume(req, res, next) {
    const payload = req.body
    try {
      const resume = await Resume.updateOne({ _id: payload._id }, payload)
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getResumes(req, res, next) {
    try {
      const resumes = await Resume.find({})
      res.json(resumes)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getResume(req, res, next) {
    const { id } = req.params
    console.log(id)
    try {
      const resumes = await Resume.findOne({ id_user: id })
      res.json(resumes)
      console.log(resumes)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = resumeController