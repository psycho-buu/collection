const userController = {
  userList: [
    // {
    //   id: 1,
    //   name: 'Jennie Kim',
    //   password: 'jj2525',
    //   email: 'Jennierubyjen@gmail.com',
    //   tel: '0955868597'
    // },
    // {
    //   id: 2,
    //   name: 'Lalisa Manoban',
    //   password: 'lala2424',
    //   email: 'lalalisa@gmail.com',
    //   tel: '0814950030'
    // }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
    return user
  },
  updateUser (user) {
    // หา index ใน userList โดยหาแบบ ไปทีละ item โดยมีเงือนไข item.id === user.id
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  deleteUser (id) {
    const index = this.userList.findIndex(item => item.id === parseInt(id))
    this.userList.splice(index, 1)
    return { id }
  },
  getUsers () {
    return [...this.userList]
  },
  getUser (id) {
    const user = this.userList.find(item => item.id === parseInt(id))
    return user
  }
}

module.exports = userController
