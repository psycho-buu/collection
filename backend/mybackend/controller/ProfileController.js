const Profile = require('../models/Profile')
const profileController = {
    profile: [{
        first_name: "สาวศศิ",
        last_name: "สว่าง",
        birth_date: "1/12/2538",
        sex: "หญิง",
        nationality: "ไทย",
        religion: "ศาสนาพุทธ",
        address: "177/2",
        province: "ชลบุร",
        district: "เมือง",
        subdistrict: "เสม็ด",
        postalnumber: "2000",
        tel: "0888888888"
    }],
     async addProfile (req, res, next){
        const payload = req.body
        // res.json(profilesController.addProfile(payload))
        const profile = new Profile(payload)
        console.log(profile)
        try{
          await profile.save()
          res.json(profile)
        }catch(err){
          res.status(500).send(err)
        }
      },
    async updateProfile(req,res,next){
        const payload = req.body
        console.log(payload)
        // res.json(profilesController.updateProfile(payload))
        try{
          const profile = await Profile.updateOne({ _id: payload._id }, payload)
          console.log(payload._id )
          res.json(profile)
        }catch(err){
          res.status(500).send(err)
        }
        
      },
    async deleteProfile(req,res,next){
        // res.json(profilesController.deleteProfile())
        try{
          const profile = await Profile.deleteMany()
          res.json(profile)
        }catch(err){
          res.status(500).send(err)
        }
      },
    async getProfiles  (req, res, next){
        try{
          const profile = await Profile.find()
          res.json(profile[0])
          console.log(profile)
        }catch(err){
          res.status(500).send(err)
        }
      },
    async getProfile (req, res, next) {
        const {id} = req.params
        try{
          const profile = await Profile.findOne({id_user:id})
          if(profile==null){
            res.json('')
          }else{
            res.json(profile)
          }
           console.log(profile)
        }catch(err){
          res.status(500).send(err)
        }
      }
}

module.exports = profileController