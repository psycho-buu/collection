const Company = require('../models/Company')
const companyController = {
  companyList: [
    // {
    //   id: 1,
    //   company_name: "CAKE",
    //   type: "ธุรกิจการค้าส่ง",
    //   address: "12/12",
    //   subdistrict: "เสม็ด",
    //   district: "เมือง",
    //   province: "ชลบุรี",
    //   postalnumber: "20130",
    //   tel: "0948612920",
    //   email: "anna@gmail.com",
    //   website: "Bookmark.com",
    //   line: "kokijung"
    // },
    // {
    //   id: 2,
    //   company_name: "Bingo",
    //   type: "ธุรกิจการค้าปลีก",
    //   address: "1002/125",
    //   subdistrict: "แสนสุข",
    //   district: "เมือง",
    //   province: "ชลบุรี",
    //   postalnumber: "20130",
    //   tel: "0948612511",
    //   email: "YOYO@gmail.com",
    //   website: "Psycho.com",
    //   line: "kochujung"
    // }
  ],
  lastId: 3,
  async addCompany(req, res, next) {
    // company.id = this.lastId++
    // this.companyList.push(company)
    // return company
    const payload = req.body
    const company = new Company(payload)
    try {
      await company.save()
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCompany(req, res, next) {
    // const index = this.companyList.findIndex(item => item.id === company.id)
    // this.companyList.splice(index, 1, company)
    // return company
    const payload = req.body
    try {
      const company = await Company.updateOne({ _id: payload._id }, payload)
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCompany(req, res, next) {
    // const index = this.companyList.findIndex(item => item.id === parseInt(id))
    // this.companyList.splice(index, 1)
    // return { id }
    const { id } = req.params
    try {
      const company = await Company.deleteOne({ _id: id })
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompanies(req, res, next) {
    // return [...this.companyList]
    try {
      const companies = await Company.find({})
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  getCompany(req, res, next) {
    // const company = this.companyList.find(item => item.id === parseInt(id))
    // return company
    const { id } = req.params
    Company.findById(id).then(function (Company) {
      res.json(Company)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
}

module.exports = companyController
