const Wishlist = require('../models/Wishlist')
const Job = require('../models/JobPost')
const CompanyProfile = require('../models/CompanyProfile')
const wishlistController = {
  wishlist: [{
    // companyName: "Ohmy",
    // jobType: "การเงิน",
    // position: "บัญชี",
    // address: "กรุงเทพ",
    // minSalary: 10000,
    // maxSalary: 20000,
  }],

  async getwishlistIdUser(req, res, next) {
    const { id } = req.params
    console.log(id)
    try {
      const wishlist = await Wishlist.find({ id_user: id })


      var wishlist0 = []

      for (i = 0; i < wishlist.length; i++) {
        var detail = {}
        const job = await Job.findOne({ _id: wishlist[i].id_jobpost })
        const company = await CompanyProfile.findOne({ id_user: job.id_user })
        detail.company_name = company.company_name
        detail.position = job.position
        detail.jobType = job.jobType
        detail._id = wishlist[i].id
        detail.id_user = wishlist[i].id_user
        detail.id_jobpost = wishlist[i].id_jobpost
        wishlist0.push(detail)
        console.log(wishlist[i])
      }

      res.json(wishlist0)
      console.log(wishlist0)

    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addtowishlist(req, res, next) {
    const payload = req.body
    // res.json(profilesController.addProfile(payload))
    const wishlist = new Wishlist(payload)
    console.log(wishlist)
    try {
      await wishlist.save()
      res.json(wishlist)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deletewishlist(req, res, next) {
    const { id } = req.params
    try {
      const wishlist = await Wishlist.deleteOne({ _id: id })
      res.json(wishlist)
    } catch (err) {
      res.status(500).send(err)
    }
  }

}

module.exports = wishlistController