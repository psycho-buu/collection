const Applicant = require('../models/Applicant')
const Job = require('../models/JobPost')
const CompanyProfile = require('../models/CompanyProfile')
const Profile = require('../models/Profile')
const applicantController = {
    Applicant: [{
  }],

  async getApplicantIdUser(req, res, next) {
    const { id } = req.params
    var iduser = id + ""
    var idjob = id + ""
    console.log(id)
    try {
      const applicant = await Applicant.find({$or:[{id_user:iduser},{id_jobpost:iduser}]})
      var wishlist0 = []

      for (i = 0; i < applicant.length; i++) {
        var detail = {}
        const job = await Job.findOne({ _id: applicant[i].id_jobpost })
        const company = await CompanyProfile.findOne({ id_user: job.id_user })
        const profile = await Profile.findOne({ id_user: applicant[i].id_user })
        detail.name = profile.first_name +" "+profile.last_name
        detail.company_name = company.company_name
        detail.position = job.position
        detail.jobType = job.jobType
        detail._id = applicant[i]._id
        detail.id_user = applicant[i].id_user
        detail.id_jobpost = applicant[i].id_jobpost
        detail.status = applicant[i].status
        detail.detail = applicant[i].detail
        wishlist0.push(detail)
        console.log(applicant[i])
      }

      res.json(wishlist0)

    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addApplicant(req, res, next) {
    const payload = req.body
    // res.json(profilesController.addProfile(payload))
    const applicant = new Applicant(payload)
    console.log(applicant)
    try {
      await applicant.save()
      res.json(applicant)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteApplicant(req, res, next) {
    const { id } = req.params
    try {
      const applicant = await Applicant.deleteOne({ _id: id })
      res.json(applicant)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateApplicant(req,res,next){
    const payload = req.body
    console.log(payload)
    try{
      const applicant = await Applicant.updateOne({ _id: payload._id }, payload)
      console.log(payload._id )
      res.json(applicant)
    }catch(err){
      res.status(500).send(err)
    }
    
  }

}

module.exports = applicantController