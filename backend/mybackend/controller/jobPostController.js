const JobPost = require('../models/JobPost')
const Applicant = require('../models/Applicant')
const jobPostController = {
  jobposts: [
    // { id: 1, name: 'งานบัญชี', minSalary: '5000', maxSalary: '10000' },
    // { id: 2, name: 'คอมพิวเตอร์/IT/โปรแกรมเมอร์', minSalary: '5000', maxSalary: '10000' }
  ],
  lastId: 3,
  async addJobPost (req, res, next) {
    const payload = req.body
    const jobpost = new JobPost(payload)
    try {
      await jobpost.save()
      res.json(jobpost)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJobPost (req, res, next) {
    const payload = req.body
    try {
      const jobpost = await JobPost.updateOne({ _id: payload._id }, payload)
      res.json(jobpost)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteJobPost (req, res, next) {
    const { id } = req.params
    try {
      const jobpost = await JobPost.deleteOne({ _id: id })
      res.json(jobpost)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPosts (req, res, next) {
    try {
      const jobposts = await JobPost.find({})
      res.json(jobposts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPost (req, res, next) {
    const { id } = req.params
    console.log(id)
    try {
      const jobposts = await JobPost.find({id_user:id})
      
      var jobpostlist = []
      for (i = 0; i < jobposts.length; i++) {
        const applicant = await Applicant.find({id_jobpost:jobposts[i]._id})
        var detail = {}
        detail.amountapplicant = applicant.length;
        detail._id = jobposts[i]._id
        detail.id_user = jobposts[i].id_user
        detail.jobType = jobposts[i].jobType
        detail.position = jobposts[i].position
        detail.open = jobposts[i].open
        detail.status = jobposts[i].status
        detail.amount =  jobposts[i].amount
        detail.military = jobposts[i].military
        detail.maxSalary = jobposts[i].maxSalary
        detail.detail = jobposts[i].detail
        detail.property = jobposts[i].property
        detail.welfare = jobposts[i].welfare
        detail.gender = jobposts[i].gender
        jobpostlist.push(detail)
      }
      console.log(jobpostlist)
      res.json(jobpostlist)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = jobPostController