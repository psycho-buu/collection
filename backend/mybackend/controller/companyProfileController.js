const CompanyProfile = require('../models/CompanyProfile')
const companyProfileController = {
  profile: [{
    company_name: "ThaiChana",
    typejob: "ธุรกิจการค้าส่ง",
    address: "1000/100",
    subdistrict: "หมีน้ำตาล",
    district: "หนองบึก",
    province: "ชลบุรี",
    postalnumber: "12345",
    tel: "0441724875",
    email: "thai@gmail.com",
    website: "chana.com",
    line: "thaichana"
  }],
  async addProfile(req, res, next) {
    const payload = req.body
    // res.json(profilesController.addProfile(payload))
    const profile = new CompanyProfile(payload)
    console.log(profile)
    try {
      await profile.save()
      res.json(profile)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateProfile(req, res, next) {
    const payload = req.body
    console.log(payload)
    // res.json(profilesController.updateProfile(payload))
    try {
      const profile = await CompanyProfile.updateOne({ _id: payload._id }, payload)
      console.log(payload._id)
      res.json(profile)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteProfile(req, res, next) {
    // res.json(profilesController.deleteProfile())
    try {
      const profile = await CompanyProfile.deleteMany()
      res.json(profile)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getProfiles(req, res, next) {
    try {
      const profile = await CompanyProfile.find()
      res.json(profile[0])
      console.log(profile)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getProfile(req, res, next) {
    const {id} = req.params
    try {
      const profile = await CompanyProfile.findOne({ id_user: id })
      if (profile == null) {
        res.json('')
      } else {
        res.json(profile)
      }
      console.log(profile)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = companyProfileController
