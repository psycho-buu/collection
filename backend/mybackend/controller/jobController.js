const Job = require('../models/JobPost')
const CompanyProfile = require('../models/CompanyProfile')
const jobController = {
  job: [{
    companyName: "Ohmy",
    jobType: "การเงิน",
    position: "บัญชี",
    address: "กรุงเทพ",
    minSalary: 10000,
    maxSalary: 20000,
  }],
  // async getJobs  (req, res, next){
  //     try{
  //       const job = await Job.find()
  //       res.json(job)
  //       console.log(job)
  //     }catch(err){
  //       res.status(500).send(err)
  //     }
  //   },
  async getJob(req, res, next) {
    const payload = req.body
    console.log(payload)
    try {
      const job = await Job.find({ $and:payload})

      var joblist = []
      for(let i = 0; i <job.length; i++){
        const company = await CompanyProfile.findOne({id_user:job[i].id_user})
        var detail = {}
        detail.company_name = company.company_name
        detail.address = company.address+" "+company.subdistrict+" "+company.district +" "+company.province+ " "+company.postalnumber
        detail._id = job[i]._id
        detail.id_user = job[i].id_user
        detail.jobType = job[i].jobType
        detail.position = job[i].position
        detail.open = job[i].open
        detail.status = job[i].status
        detail.amount =  job[i].amount
        detail.military = job[i].military
        detail.maxSalary = job[i].maxSalary
        detail.detail = job[i].detail
        detail.property = job[i].property
        detail.welfare = job[i].welfare
        detail.gender = job[i].gender
        joblist.push(detail)
        
      }

      res.json(joblist)
      console.log(joblist)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobId(req, res, next) {
    const { id } = req.params
    try {
     
      const job = await Job.findById(id)
      const company = await CompanyProfile.findOne({id_user:job.id_user})
      var detail = [job,company]
      res.json(detail)
      console.log(detail)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobs(req, res, next) {
    const payload = req.body
    console.log(payload)
    try {
      const job = await Job.find({status:"true"})
      var joblist = []
      for(let i = 0; i <job.length; i++){
        const company = await CompanyProfile.findOne({id_user:job[i].id_user})
        var detail = {}
        detail.company_name = company.company_name
        detail.address = company.address+" "+company.subdistrict+" "+company.district +" "+company.province+ " "+company.postalnumber
        detail._id = job[i]._id
        detail.id_user = job[i].id_user
        detail.jobType = job[i].jobType
        detail.position = job[i].position
        detail.open = job[i].open
        detail.status = job[i].status
        detail.amount =  job[i].amount
        detail.military = job[i].military
        detail.maxSalary = job[i].maxSalary
        detail.minSalary = job[i].minSalary
        detail.detail = job[i].detail
        detail.property = job[i].property
        detail.welfare = job[i].welfare
        detail.gender = job[i].gender
        joblist.push(detail)
      }
      res.json(joblist)
      console.log(joblist)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addJob(req, res, next) {
    const payload = req.body
    // res.json(profilesController.addProfile(payload))
    const job = new Job(payload)
    console.log(job)
    try {
      await job.save()
      res.json(job)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJob(req, res, next) {
    const payload = req.body
    // res.json(profilesController.addProfile(payload))
    console.log(payload)
    try {
      const job = await Job.updateOne({ _id: payload._id }, payload)
      res.json(job)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteJob(req, res, next) {
    const { id } = req.params
    try {
      const job = await Job.deleteOne({ _id: id })
      res.json(job)
    } catch (err) {
      res.status(500).send(err)
    }
  }

}

module.exports = jobController