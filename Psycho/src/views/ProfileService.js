const ProfileService = {
    profile: {
        first_name: "สาวศศิ",
        last_name: "สว่าง",
        birth_date: "1/12/2538",
        sex: "หญิง",
        nationality: "ไทย",
        religion: "ศาสนาพุทธ",
        address: "177/2",
        province: "ชลบุร",
        district: "เมือง",
        subdistrict: "เสม็ด",
        postalnumber: "2000",
        tel: "0888888888"
    },
    lastId: 3,
    addProfile(profile) {
        this.profile = profile;
    },
    updateProfile(profile) {
        this.profile = "";
        this.profile = profile;
    },
    deleteProfile(profile) {
        this.profile = "";
    },
    getProfile() {
        return this.profile;
    }
}

export default ProfileService
