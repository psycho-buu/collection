const companyProfileService = {
  profile: {
    company_name: "ThaiChana",
    typejob: "ธุรกิจการค้าส่ง",
    address: "1000/100",
    subdistrict: "หมีน้ำตาล",
    district: "หนองบึก",
    province: "ชลบุรี",
    postalnumber: "12345",
    tel: "0441724875",
    email: "thai@gmail.com",
    website: "chana.com",
    line:"thaichana"
  },
  lastId: 3,
  addProfile(profile) {
    // this.profile[0] = profile;
    this.profile = profile;
  },
  updateProfile(profile) {
    // this.profile.splice(0, 1, profile)
    this.profile = "";
    this.profile = profile;
  },
  deleteProfile(profile) {
    // this.profile.splice(0)
    this.profile = "";
  },
  getProfile() {
    // return this.profile[0]
    return this.profile;
  }
}

export default companyProfileService
