const JobPostService = {
  postList: [
    {
      id: 1,
      position: 'งานบัญชี',
      minSalary: 10000,
      maxSalary: 25000,
      open: '23/2/2021',
      status: false,
      amount: 5,
      military: 'ผ่าน',
      detail:
        '- ทำหน้าที่บันทึกข้อมูลทางการเงินตามระบบของการบัญชี \n- ทำบัญชีรายรับบัญชีรายจ่าย ให้กับองค์กร',
      property: '- เพศหญิง',
      welfare: '- อาหารฟรี'
    },
    {
      id: 2,
      position: 'คอมพิวเตอร์/IT/โปรแกรมเมอร์',
      minSalary: 25000,
      maxSalary: 40000,
      open: '25/2/2021',
      status: true,
      amount: 2,
      military: 'ไม่กำหนด',
      detail: 'ทำทุกอย่างไปก่อน',
      property: '- เพศหญิง\n- วุฒิปริญญาตรีขึ้นไป',
      welfare: '- ประกันสังคม'
    }
  ],
  lastId: 3,
  addPost(post) {
    post.id = this.lastId++
    this.postList.push(post)
  },
  updatePost(post) {
    const index = this.postList.findIndex(item => item.id === post.id)
    this.postList.splice(index, 1, post)
  },
  getJobPosts() {
    console.log('erng')
    return [...this.postList]
  }
}

export default JobPostService