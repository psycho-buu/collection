const companyService = {
  companyList: [
    {
      id : 1,
      company_name: "CAKE",
      type: "ธุรกิจการค้าส่ง",
      address: "12/12",
      subdistrict: "เสม็ด",
      district: "เมือง",
      province: "ชลบุรี",
      postalnumber: "20130",
      tel: "0948612920",
      email: "anna@gmail.com",
      website: "Bookmark.com",
      line:"kokijung"
    },
    {
      id : 2,
      company_name: "Bingo",
      type: "ธุรกิจการค้าปลีก",
      address: "1002/125",
      subdistrict: "แสนสุข",
      district: "เมือง",
      province: "ชลบุรี",
      postalnumber: "20130",
      tel: "0948612511",
      email: "YOYO@gmail.com",
      website: "Psycho.com",
      line:"kochujung"
    }
  ],
  lastId : 3,
  addCompany (company) {
    company.id = this.lastId++
    this.companyList.push(company)
  },
  updateCompany (company) {
    const index = this.companyList.findIndex(item => item.id === company.id)
    this.companyList.splice(index, 1, company)
  },
  deleteCompany (company) {
    const index = this.companyList.findIndex(item => item.id === company.id)
    this.companyList.splice(index, 1)
  },
  getCompanys () {
    return [...this.companyList]
  }
}

export default companyService
