import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/position',
        name: 'position',
        component: () =>
            import ('../views/position.vue')
    },
    {
        path: '/user',
        name: 'user',
        component: () =>
            import ('../views/User.vue')
    },
    {
        path: '/wishlist',
        name: 'wishlist',
        component: () =>
            import ('../views/Wishlist.vue')
    },
    {
        path: '/appliedjob',
        name: 'appliedjob',
        component: () =>
            import ('../views/Appliedjob.vue')
    },
    {
        path: '/applicant',
        name: 'applicant',
        component: () =>
            import ('../views/Applicant.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: () =>
            import ('../views/Register.vue')
    },
    {
        path: '/profile',
        name: 'profile',
        component: () =>
            import ('../views/Profile.vue')
    },
    {
        path: '/resume',
        name: 'resume',
        component: () =>
            import ('../views/Resume.vue')
    },
    {
        path: '/company',
        name: 'company',
        component: () =>
            import ('../views/Company.vue')
    },
    {
        path: '/jobpost',
        name: 'jobpost',
        component: () =>
            import ('../views/JobPost.vue')
    },
    {
        path: '/searchjob',
        name: 'searchjob',
        component: () =>
            import ('../views/Searchjob.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: () =>
            import ('../views/Login.vue')
    },
    {
        path: '/jobdetail/:postId',
        name: 'jobdetail',
        component: () =>
            import ('../views/JobDetail.vue')
    },
    {
        path: '/companyprofile',
        name: 'companyprofile',
        component: () =>
            import ('../views/CompanyProfile.vue')
    },
    {
        path: '/testlogin',
        name: 'testlogin',
        component: () =>
            import ('../views/TestLogin.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router